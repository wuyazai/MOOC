## 创建型设计模式-Singleton单例模式

单例模式有3个特点:

1. 单例对象可以保证在一个 JVM 中只存在一个实例对象；
2. 该单例对象必须由单例类自行创建；
3. 单例类对外提供一个访问单例对象的全局方法；

单例模式的优点:

1. 单例模式可以保证内存里只有一个实例、减少了内存的开销；
2. 可以避免对资源的多重占用；
3. 单例模式设置全局访问点、可以优化和共享资源的访问；

单例模式结构:

1. 私有的静态的实例对象 private static Instance；
2. 私有的构造函数 private Singleton(){} 保证在该类外部、无法通过new的方式来创建对象实例；
3. 公有的、静态的、访问单例对象实例的方法 public static Singleton getInstance(){ }

### 懒加载形式

```java
/**
 * 懒加载形式
 */
public final class SingletonLazy {

    private SingletonLazy() {
    }

    private static Student student = null;

    public static Student getInstance() {
        if (null == student) {
            student = new Student("人民", "22", "男", "China@mail");
        }
        return student;
    }
}
```

```java
/**
 * 由于CPU时间切片,懒加载形式是线程不安全的；
 * 描述：如果该单例类被首次调用时就有并发,SingletonLazy.getInstance()方法很有可能会产生多个实例。这是与单例模式的初衷相违背的。
 *
 * 如下测试:
 */
public class Client {
    public static void main(String[] args) {
        int num = 10;
        // 模拟并发
        CountDownLatch countDown = new CountDownLatch(num);
        // 构建线程池 
        ThreadPoolExecutor singletonThread = new ThreadPoolExecutor(
                10,
                20,
                5000,
                TimeUnit.MICROSECONDS,
                new ArrayBlockingQueue<Runnable>(30),
                ThreadFactoryBuilder.create().get());

        for (int i = 0; i < num; i++) {
            singletonThread.submit(() -> {
                // 倒数
                countDown.countDown();
                try {
                    // 当前线程等待倒数完成
                    countDown.await();
                    // 具体任务
                    Student instance = SingletonLazy.getInstance();
                    System.out.println("线程名: " + Thread.currentThread().getName() + " 对象地址: " + instance);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        // 关池
        singletonThread.shutdown();
    }
}
```

#### 基于volatile的解决方案

```java
/**
 * 前置知识
 *      1.双重检查锁定与延迟初始化：
 *          https://www.infoq.cn/article/double-checked-locking-with-delay-initialization/
 *      2.重排序：
 *          https://segmentfault.com/a/1190000038355290
 *      3.单线程规范：
 *          intra-thread semantics (线程内语义)
 *          保证重排序不会改变单线程内程序的执行结果,但是多线程会有问题。
 *      4.Volatile关键字：
 *          http://tutorials.jenkov.com/java-concurrency/volatile.html
 *      5.类初始化的LC锁：
 *          http://ifeve.com/initialization-on-demand-holder-idiom/
 *          https://tuonioooo-notebook.gitbook.io/java-concurrent/di-san-zhang-java-nei-cun-mo-xing/shuang-zhong-jian-cha-suo-ding-yu-yan-chi-chu-shi-hua
 */
public final class SingletonLazySafe {

    private SingletonLazySafe() {
    }

    private static volatile Student student = null;

    /**
     * 加一个锁保证线程安全。但是在此位置加锁会造成：
     *      即使student已经被实例化过了,在并发场景下仍然会阻塞线程,造成性能问题,这显然不是我们想看到的。
     */
    public static synchronized Student getInstance() {
        if (null == student) {
            // 也会有“重排序造成的问题”
            student = new Student("人民", "22", "男", "China@mail");
        }
        return student;
    }

    /**
     * 直接使用"双重检查锁定"也会有问题,因为计算机指令会有"重排序"优化,并发时会造成某些线程访问到未初始化的对象。
     *      解决思路：
     *          1.禁止重排序,用volatile修饰变量来禁止重排序。
     *          2.允许重排序,但需要使类初始化过程对"非构造线程封闭",可以借用静态内部类的方式来实现。技术实质是利用Java类初始化的LC锁。
     */
    public static Student getInstanceSyn() {
        // 第一次检查
        if (null == student) {
            synchronized (SingletonLazySafe.class) {
                // 第二次检查
                if (null == student) {
                    // 会有“重排序造成的问题”
                    student = new Student("人民", "22", "男", "China@mail");
                }
            }
        }
        return student;
    }

}
```

#### 基于类初始化的解决方案

```java
/**
 * 静态内部类,实现单例懒汉模式,是线程安全的
 *      这个方案称之为："按需初始化持有者习惯用法" (Initialization On Demand Holder idiom) 技术实质是利用Java类初始化的LC锁。
 */
public final class SingletonLazySafeInnerStatic {

    private SingletonLazySafeInnerStatic() {
    }

    /**
     * 静态内部类
     *      1.外部类被加载时,静态内部类不会被加载,被调用才会加载,他们是相互独立的。而普通内部类是属于外部类的一部分,故不适用于懒汉模式。
     *      2.静态内部类在初始化时是线程安全的,JVM内部会保证只有一个线程去执行对象初始化指令。
     */
    public static class SingletonHoder {
        private static final Student HODER = new Student("人民", "22", "男", "China@mail");
    }

    /**
     * 不需要加锁
     */
    public static Student getInstance() {
        return SingletonHoder.HODER;
    }

}
```