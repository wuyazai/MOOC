package com.mooc.headfirst.create.Single.lazy;

/**
 * <p>
 *  创建型设计模式：懒汉单例模式 懒汉的方式会有线程安全问题
 * </p>
 * @author kk
 * @date 2021/12/2 16:59
 */
public final class SingletonLazy {

    private SingletonLazy() throws InterruptedException {

    }

    private static Student student = null;

    public static Student getInstance() {
        if (null == student) {
            student = new Student("人民", "22", "男", "China@mail");
        }
        return student;
    }
}
