package com.mooc.headfirst.create.FactoryMethod;


/**
 * 玩具商店
 */
public class Test {

    public static void main(String[] args) {

    }
}


// 工厂
abstract class Store {

    public final Pizza orderPizza(String type) {
        Pizza pizza;
        pizza = createPizza(type);
        pizza.bake();
        return pizza;
    }

    // 工厂方法
    abstract Pizza createPizza(String type);

}

// 产品
abstract class Pizza {

    // 披萨都有一些共同的属性 都需要名字 面团 酱料

    // 披萨名字
    String name;

    // 面团
    String dough;

    // 酱
    String sauce;

    void bake() {
        System.out.println("披萨 烤制");
    }

    void cut() {
        System.out.println("披萨 切块");
    }

    void box() {
        System.out.println("披萨 装盒");
    }

}