package com.mooc.headfirst.create.Single.lazy;

import java.util.concurrent.*;

/**
 * <p>
 * 测试懒汉模式
 * </p>
 *
 * @author kk
 * @date 2021/12/7 11:12
 */
public class Client {
    public static void main(String[] args) {
        int num = 10;
        // 模拟并发
        CountDownLatch countDown = new CountDownLatch(num);
        // 构建线程池
        ThreadPoolExecutor singletonThread = new ThreadPoolExecutor(
                10,
                20,
                5000,
                TimeUnit.MICROSECONDS,
                new ArrayBlockingQueue<>(30),
                Executors.defaultThreadFactory());

        for (int i = 0; i < num; i++) {
            singletonThread.submit(() -> {
                // 倒数
                countDown.countDown();
                try {
                    // 当前线程等待倒数完成
                    countDown.await();
                    // 具体任务
                    Student instance = SingletonLazySafe.getInstanceSyn();
                    System.out.println("线程名: " + Thread.currentThread().getName() + " 对象地址: " + instance);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        // 关池
        singletonThread.shutdown();
    }
}
