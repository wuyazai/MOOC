package com.mooc.headfirst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author likun
 */
@SpringBootApplication
public class HeadFirstApplication {
    public static void main(String[] args) {
        SpringApplication.run(HeadFirstApplication.class, args);
    }
}
