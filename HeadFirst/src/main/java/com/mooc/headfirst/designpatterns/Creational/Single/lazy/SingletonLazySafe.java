package com.mooc.headfirst.designpatterns.Creational.Single.lazy;

/**
 * 双重检查锁定与延迟初始化:
 *      https://www.infoq.cn/article/double-checked-locking-with-delay-initialization/
 *
 * 重排序:
 *      https://segmentfault.com/a/1190000038355290
 *
 * 单线程规范 intra-thread semantics (线程内语义):
 *      保证重排序不会改变单线程内程序的执行结果,但是多线程会有问题。
 *
 * 直接使用双重检查锁定也会有问题,由于对象实例创建时会有“重排序”,并发时会造成某些线程访问到未初始化的对象。
 * 解决思路：
 *      1.禁止重排序,用volatile修饰变量来禁止重排序。
 *      2.允许重排序,但初始化过程对“非构造线程封闭”,可以使用静态内部类的方式。
 *
 * volatile关键字：
 *      https://www.cnblogs.com/dolphin0520/p/3920373.html
 *      http://tutorials.jenkov.com/java-concurrency/volatile.html
 *
 * 多线程并发环境下,类初始化为什么会线程安全：
 *      http://ifeve.com/initialization-on-demand-holder-idiom/
 *      https://tuonioooo-notebook.gitbook.io/java-concurrent/di-san-zhang-java-nei-cun-mo-xing/shuang-zhong-jian-cha-suo-ding-yu-yan-chi-chu-shi-hua
 * <p>
 * 线程安全的懒汉单例模式
 * </p>
 *
 * @author kk
 * @date 2021/12/7 14:05
 */
public final class SingletonLazySafe {

    private SingletonLazySafe() {
    }

    private static volatile Student student = null;

    /**
     * 一般的方式:
     * 加一个锁保证线程安全。但是在此位置加锁会造成：
     *      即使student已经被实例化过了,在并发场景下仍然会有大量线程阻塞,造成性能问题,这显然不是我们想看到的。
     *      可以使用“双重检查锁定”来规避一部分压力,详情见下文。
     */
    public static synchronized Student getInstance() {
        if (null == student) {
            // 也会有重排序问题,变量仍然需要加volatile
            student = new Student("人民", "22", "男", "China@mail");
        }
        return student;
    }

    /**
     * 双重检查锁定的方式:
     * @return student
     */
    public static Student getInstanceSyn() {
        // 第一次判断
        if (null == student) {
            synchronized (SingletonLazySafe.class) {
                // 第二次判断
                if (null == student) {
                    // 会有“重排序造成的问题”
                    student = new Student("人民", "22", "男", "China@mail");
                }
            }
        }
        return student;
    }
}
