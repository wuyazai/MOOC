package com.mooc.headfirst.designpatterns.Creational.Single.hungry;

import com.mooc.headfirst.designpatterns.Creational.Single.lazy.Student;

/**
 * <p>
 *  饿汉式的即时加载不会有线程安全问题
 * </p>
 *
 * @author kk
 * @date 2021/12/29 9:13
 */
public class HungrySingleton {

    private static final Student INSTANCE = new Student();

    private HungrySingleton() {

    }

    public static Student getInstance() {
        return INSTANCE;
    }
}
