package com.mooc.headfirst.designpatterns.Creational.Single.lazy;

/**
 * <p>
 * Effective Java 作者推荐的方式,也是最好的方式,可以保证线程安全问题,能防止序列化攻击和反射攻击
 *      https://juejin.cn/post/6955698964993671182
 *
 * 防止攻击：
 *      https://juejin.cn/post/6844904186463649800
 *      https://juejin.cn/post/6844904186027458568
 *
 * 反射破坏和序列化破坏：
 *      https://blog.csdn.net/siying8419/article/details/82152921
 * </p>
 *
 * @author kk
 * @date 2021/12/29 10:16
 */
public class SingletonLazySafeEnum {

    /**
     * 构造方法私有化 防止直接通过类创建实例
     */
    private SingletonLazySafeEnum() {
    }

    public static Student getInstance() {
        return SinletonHolder.INSTANCE.getInstance();
    }


    /**
     * 枚举类
     */
    private enum SinletonHolder {

        /**
         * 枚举
         */
        INSTANCE;

        /**
         * 要创建的对象
         */
        private final Student instance;

        /**
         * jvm会保证枚举类的构造函数只被执行一次
         */
        SinletonHolder() {
            instance = new Student();
        }

        /**
         * 枚举类里的方法需要通过枚举值进行调用
         *
         * @return student
         */
        private Student getInstance() {
            return instance;
        }
    }

}
