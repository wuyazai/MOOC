package com.mooc.headfirst.designpatterns.Creational.Single.lazy;

import java.util.Objects;

/**
 * <p>
 *
 * </p>
 *
 * @author kk
 * @date 2021/12/7 10:53
 */
public class Student {
    private String name;
    private String age;
    private String gender;
    private String mail;

    public Student() {}

    public Student(String name, String age, String gender, String mail) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.mail = mail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

}
