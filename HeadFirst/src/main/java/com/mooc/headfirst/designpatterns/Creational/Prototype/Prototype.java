package com.mooc.headfirst.designpatterns.Creational.Prototype;

import java.util.HashMap;

/**
 * <p>
 *  原型模式
 *  此设计模式的意图：通过复制一个现有的对象来生成一个新的对象，而不是通过实例化的方式。
 * </p>
 *
 * @author kk
 * @date 2021/12/29 10:09
 */
public class Prototype {

    public static void main(String[] args) {
        HashMap<String, String> string = new HashMap<>(64);
    }

}
