package com.mooc.headfirst.designpatterns.Creational.Single.lazy;

/**
 * <p>
 *  通过静态内部类 来解决并发问题
 * </p>
 *
 * @author kk
 * @date 2021/12/8 9:51
 */
public final class SingletonLazySafeInnerStatic {

    private SingletonLazySafeInnerStatic() {}

    /**
     * 静态内部类
     *      1.外部类被加载时,静态内部类不会被加载,被调用才会加载,他们是相互独立的。而普通内部类是属于外部类的一部分,故不适用于懒汉模式。
     *      2.静态内部类在初始化时是线程安全的,JVM内部会保证,只会有一个线程去执行初始化指令。
     */
    public static class SingletonHoder {
        private static final Student HODER = new Student("人民", "22", "男", "China@mail");
    }

    /**
     * 不需要加锁
     */
    public static Student getInstance() {
        return SingletonHoder.HODER;
    }

}
