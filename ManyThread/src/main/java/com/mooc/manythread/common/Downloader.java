package com.mooc.manythread.common;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;


public class Downloader {

    static final RestTemplate restTemplate = new RestTemplate();

    public static String download() {
//        try {
//            TimeUnit.SECONDS.sleep(5);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        // 支持处理text/html结果集的Converter
        HttpMessageConverter<?> converter = new StringHttpMessageConverter(StandardCharsets.UTF_8);
        // RestTemplate的messageConverters是一个集合，里面有很多Converter，其中StringHttpMessageConverter所在下标为1，但是它默认是“ISO-8859-1”字符集，会乱码，所以要替换它。
        restTemplate.getMessageConverters().set(1, converter);
        ResponseEntity<String> forEntity = restTemplate.getForEntity("https://www.baidu.com/", String.class);
        return forEntity.getBody();
    }
}
