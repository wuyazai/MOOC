package com.mooc.manythread.manythread;

import java.util.ArrayList;
import java.util.List;

/**
 * description:
 * author: lK
 * time: 2021/5/31 16:22
 *
 * @author likun
 */
public class ManyThreadAndJava8Stream {
    public static void main(String[] args) throws Exception {

        // 并行流的问题 多个线程抢占往container里面add而container又是线程不安全的。所以container的size会出现小于source.size()的情况。
        // 线程不安全问题是概率出现,所以循环100次

        // container 是共享变量，ArrayList不是线程安全的。add方法是往一个<Object[] elementData> 数组里面放值。
        // 比如多个线程同时往数组 elementData[5] 放值，那么后面线程肯定会把前面线程的操作覆盖掉。而且偶尔还会出现数组下标越界ArrayIndexOutOfBoundsException。详情查看add方法。
        for (int i = 0; i < 50; i++) {
            List<String> source = new ArrayList<>();
            List<String> container = new ArrayList<>(); //多个线程去抢占container
            source.add("a");
            source.add("b");
            source.add("c");
            source.add("d");
            source.add("e");
            source.add("f");
            source.add("g");
            source.add("h");
            source.add("i");
            source.add("j");
            source.add("k");

            source.parallelStream().forEach(container::add);
            System.out.println(container.size()+"--具体数据-->"+container.toString());
        }

        // 这样就不会报错了
//        for (int i = 0; i < 50; i++) {
//            List<String> source = new ArrayList<>();
//            List<String> container = Collections.synchronizedList(new ArrayList<String>());
//            source.add("a");
//            source.add("b");
//            source.add("c");
//            source.add("d");
//            source.add("e");
//            source.add("f");
//            source.add("g");
//            source.add("h");
//            source.add("i");
//            source.add("j");
//            source.parallelStream().forEach(container::add);
//            System.out.println(container.size());
//        }

    }

}
