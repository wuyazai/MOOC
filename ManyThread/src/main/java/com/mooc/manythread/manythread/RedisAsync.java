package com.mooc.manythread.manythread;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class RedisAsync {

    // corePoolSize 常驻线程数
    // maximumPoolSize 最大线程数
    // keepAliveTime 当线程数超过corePoolSize时，空闲线程的存活时间，释放掉不用的线程
    // unit 单位，keepAliveTime的单位
    // workQueue 任务的队列，BlockingQueue
    // threadFactory 线程工厂，一般使用默认的
    // handler 拒绝策略，RejectedExecutionHandler
    ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
            5,
            10,
            5,
            TimeUnit.SECONDS,
            new ArrayBlockingQueue<>(5),
            Executors.defaultThreadFactory(),
            new ThreadPoolExecutor.AbortPolicy()
    );

    public static void execute(Runnable consumer) {
        // consumer 这个任务交给线程池去执行
    }

}
