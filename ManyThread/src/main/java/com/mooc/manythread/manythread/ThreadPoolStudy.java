package com.mooc.manythread.manythread;

import java.util.concurrent.*;

/**
 * 线程池学习
 * Java 中的线程池是通过 Executor 框架实现的，该框架中用到了 Executor，Executors，ExecutorService，ThreadPoolExecutor，Callable 和 Future，FutureTask 这几个类
 * <p>
 * 任务被提交，线程池会新建核心线程处理任务
 * 当核心线程都被占用，新来的任务会放到任务队列里
 * 当任务队列满了，就会创建新的线程来处理任务
 * 如果线程池达到了最大线程数，任务将会被拒绝
 */
public class ThreadPoolStudy {

    public static void main(String[] args) {
        ExecutorService cachedExecutor = Executors.newCachedThreadPool();
        ExecutorService fixedExecutor = Executors.newFixedThreadPool(5);
        ExecutorService singleExecutor = Executors.newSingleThreadExecutor();
        ScheduledExecutorService scheduledExecutor = Executors.newScheduledThreadPool(5);

        // corePoolSize 常驻线程数
        // maximumPoolSize 最大线程数
        // keepAliveTime 当线程数超过corePoolSize时，空闲线程的存活时间
        // unit 单位，keepAliveTime的单位
        // workQueue 任务的队列，BlockingQueue
        // threadFactory 线程工厂，一般使用默认的
        // handler 拒绝策略，RejectedExecutionHandler
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                5,
                10,
                5,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(5),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy()
        );

    }


}
