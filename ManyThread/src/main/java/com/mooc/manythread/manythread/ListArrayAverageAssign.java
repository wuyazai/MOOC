package com.mooc.manythread.manythread;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * description: 多线程拆分大集合为小集合
 *
 * @author likun
 */
public class ListArrayAverageAssign {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        // 循环次数太多把for循环分批,然后使用多线程
        // 测试拆分方法
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            integers.add(i);
        }
        List<List<Integer>> lists = averageAssign(integers, 20);
        System.out.println(lists);


        // 多线程CountDownLatch使用
        final CountDownLatch count = new CountDownLatch(lists.size());

        long start = System.currentTimeMillis();

        for (int i = 0; i < lists.size(); i++) {
            new Thread(() -> {
                try {
                    System.out.println("我是线程：" + Thread.currentThread().getName());
                    TimeUnit.SECONDS.sleep(5);
                    count.countDown();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        }

        // main线程等待子线程执行完毕
        count.await();
        System.out.println("我是主线程,子线程执行完毕");

        long end = System.currentTimeMillis();
        System.out.println("执行时间:" + (end - start));
    }


    /**
     * @param list 线程池处理拆分后的集合
     * @param <T>  实体类
     * @return 处理后的
     * @throws ExecutionException   异常
     * @throws InterruptedException 异常
     *                              <p>
     *                              Future Java1.8之前的一种异步获取线程执行结果的方式 * 有些鸡肋 * 可以采用 CompletableFuture
     */
    public <T> List<?> taskExecution(List<List<T>> list) throws ExecutionException, InterruptedException {
        int n = list.size();

        CountDownLatch countDown = new CountDownLatch(n);

        ExecutorService executorService = Executors.newFixedThreadPool(n);

        ArrayList<?> futures = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            int indexI = i;

            executorService.submit(() -> {

                list.get(indexI).forEach(item -> {

                });

                countDown.countDown();
            });
        }

//        execute方法和submit方法有什么区别
//        executorService.execute(() -> {
//        });

        // main线程等待以上子线程执行完毕
        countDown.await();

        // 关池
        executorService.shutdown();
        return futures;
    }


    /**
     * 把一个大集合分成n个小集合
     *
     * @param source 大集合
     * @param n      要成分成多少个小集合 * n值最好为当前操作系统cpu核心数量的两倍
     * @param <T>    泛型
     * @return 拆分后的小集合
     */
    public static <T> List<List<T>> averageAssign(List<T> source, int n) {
        ArrayList<List<T>> result = new ArrayList<>();
        // 取余数
        int remainder = source.size() % n;
        // 取商
        int quotient = source.size() / n;
        // 偏移量
        int offset = 0;

        for (int i = 0; i < n; i++) {
            List<T> value = null;
            if (remainder > 0) {
                value = source.subList(i * quotient + offset, (i + 1) * quotient + offset + 1);
                remainder--;
                offset++;
            } else {
                value = source.subList(i * quotient + offset, (i + 1) * quotient + offset);
            }
            result.add(value);
        }
        return result;
    }

}
