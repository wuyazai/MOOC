package com.mooc.manythread.manythread;

import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Volatile 关键字 1禁止重排序 2保证线程间共享变量的可见性
 * <p>
 * 线程的sleep()方法还有个含义: 暂时放弃cpu的执行权
 * <p>
 * 1.奇怪的地方是这个案例中 当循环体内部有代码时 程序会停止
 * 2.在线程内部创建一个字符串 String s = "hello" 然后输出,他第一次输出只输出了llo,单词输出不全
 */
public class ManyThreadVolatile {

//    private static Boolean m = true;
//
//    public static void main(String[] args) throws InterruptedException {
//        new Thread(() -> {
//            while (m) {
//                // to do
//            }
//            System.out.println("end");
//        }, "Thread H").start();
//
//        TimeUnit.SECONDS.sleep(1);
//        m = false;
//    }

//    private static boolean m = true;
//
//    public static void main(String[] args) throws InterruptedException {
//        new Thread(() -> {
//            while (m) {
//                System.out.println("布尔值是: " + m);
//            }
//            System.out.println("end");
//        }, "Thread H").start();
//
//        TimeUnit.SECONDS.sleep(1);
//        m = false;
//        System.out.println("已更改变量");
//    }


//    private static Boolean m = true;
//
//    public static void main(String[] args) throws InterruptedException {
//        new Thread(() -> {
//            for (int i = 0; i < 1000; i++) {
//                if (m) {
//                    try {
//                        Thread.sleep(10);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    System.out.println("yes" + i);
//                } else {
//                    break;
//                }
//            }
//        }, "Thread H").start();
//
//        TimeUnit.SECONDS.sleep(1);
//        m = false;
//    }


//    private static boolean m = true;
//
//    public static void main(String[] args) throws InterruptedException {
//        new Thread(() -> {
//            while (m) {
//                ArrayList<Object> objects = new ArrayList<>();
//                objects.add("hello");
//                Object o = objects.get(0);
//            }
//            System.out.println("end");
//        }, "Thread H").start();
//
//        Thread.sleep(1000);
//        m = false;
//    }


//    public static boolean n = true;
//
//    public static void main(String[] args) throws InterruptedException {
//
//        new Thread(() -> {
//            while (n) {
//                // to do
//                System.out.println("我是" + Thread.currentThread().getName() + " 布尔值是：" + n);
//            }
//        }, "thread 01").start();
//
//        TimeUnit.SECONDS.sleep(1);
//
//        new Thread(() -> {
//            while (n) {
//                n = false;
//                System.out.println("我是" + Thread.currentThread().getName() + " 布尔值是：" + n);
//            }
//        }, "thread 02").start();
//    }


//    private static boolean m = true;
//
//    public static void main(String[] args) throws InterruptedException {
//        new Thread(() -> {
//            while (m) {
//                try {
//                    Thread.sleep(10);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//            System.out.println("end");
//        }, "Thread H").start();
//
//        TimeUnit.SECONDS.sleep(1);
//        m = false;
//    }


//    public static void main(String[] args) throws InterruptedException {
//
//        final boolean[] n = {true};
//
//        new Thread(() -> {
//            while (n[0]) {
//                // to do
//                System.out.println("我是" + Thread.currentThread().getName() + " 布尔值是：" + n[0]);
//            }
//        }, "thread 01").start();
//
//        TimeUnit.SECONDS.sleep(1);
//
//        new Thread(() -> {
//            while (n[0]) {
//                n[0] = false;
//                System.out.println("我是" + Thread.currentThread().getName() + " 布尔值是：" + n[0]);
//            }
//        }, "thread 02").start();
//    }

}
