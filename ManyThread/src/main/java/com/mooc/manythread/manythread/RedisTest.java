package com.mooc.manythread.manythread;

import org.springframework.data.redis.core.RedisTemplate;

/**
 * redis 的操作异步去执行
 */
public class RedisTest {

    static RedisTemplate<String, String> redisTemplate = new RedisTemplate<String, String>();

    public static void main(String[] args) {
        RedisAsync.execute(() -> {
            redisTemplate.opsForHash().put("key", "hashKey", "hashValue");
        });
    }
}
