package com.mooc.manythread.heima.unit4;

import com.mooc.manythread.common.Student;

import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.concurrent.TimeUnit;

/**
 * ThreadLocal拷贝的变量副本是深克隆还是浅克隆
 * <p>
 * 答案：是浅克隆
 * <p>
 * 详解：
 * 浅克隆
 * 仅仅复制了对象本身，对象内部的成员变量还是引用的原来的。换言之，浅复制仅仅复制所考虑的对象，而不复制对象变量所引用的对象。
 * <p>
 * 深克隆
 * 深复制就是把对象本身和它的成员变量所引用的对象都复制了一遍。
 */
public class HeimaThreadCreate05 {

    private final static Student person = new Student("李锟锟", "河南省，郑州市");

    /**
     * 深克隆浅克隆测试 这个理解是错误的请查看详解
     *
     * @param args 参数
     * @throws InterruptedException 异常
     */
    public static void main(String[] args) throws InterruptedException {

        Student student = null;
        student = person;
        student.setAddress("古城");

        // 修改了student原来的person也被修改了
        System.out.println("student: " + student);
        System.out.println("person: " + person);

    }


}
