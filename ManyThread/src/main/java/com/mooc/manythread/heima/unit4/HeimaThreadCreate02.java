package com.mooc.manythread.heima.unit4;

/**
 * 深入synchronized关键字
 * * synchronized关键字，底层的三种锁机制。
 * * <a href="https://blog.csdn.net/Colorful_X/article/details/117110786">...</a>
 * * <a href="https://tech.meituan.com/2018/11/15/java-lock.html">...</a> 美团技术分享
 * <p>
 * Java的对象头，Java中的对象都有一个对象头Object Header（Mark Word，Kclass Word）
 * 对象头由两部分组成，一部分用于存储自身的运行时数据，称之为Mark Word。另外一部分是类型指针。
 * <p>
 * 重要前置知识：
 * <a href="https://blog.csdn.net/qq_36434742/article/details/106854061">...</a>
 * Java当中的上锁是通过改变对象头（Mark Word）的状态来实现的。Mark Word主要有无锁状态，偏向状态，轻量级锁状态（此状态的 Mark Word是一个指针），重量级锁状态，GC标记状态这几种。
 *
 * <p><p><p><p><p><p><p><p><p><p>
 * 重量级锁
 * 每一个被锁住的对象都会和一个Monitor关联，同时Monitor中有一个Owner字段存放拥有该锁的线程的唯一标识，表示该锁被这个线程占用。锁被占用时来了线程，线程会被阻塞，进入Monitor的阻塞队列（EntryList）。
 * Monitor 管程（监视器）由虚拟机创建，与操作系统交互。Monitor有三个重要字段 WaitSet（等待池），EntryList（阻塞队列），Owner。
 * <p>
 * 美团博客：
 * 重量级锁是依赖对象内部的Monitor（监视器/管程）来实现的，而Monitor又依赖于操作系统底层的 Mutex Lock （互斥锁）实现，
 * “阻塞或唤醒一个Java线程需要操作系统切换CPU状态来完成，这种状态转换需要耗费处理器时间。如果同步代码块中的内容过于简单，
 * 状态转换消耗的时间有可能比用户代码执行的时间还要长”。这种方式就是synchronized最初实现同步的方式，这就是JDK 6之前synchronized效率低的原因。
 * 这种依赖于操作系统Mutex Lock所实现的锁我们称之为“重量级锁”，JDK 6中为了减少获得锁和释放锁带来的性能消耗，引入了“偏向锁”和“轻量级锁”。
 * <p>
 * 重量级锁的自旋优化：
 * 发生了锁竞争时，线程不会立即进入阻塞队列（EntryList），会再次去尝试获取锁（自旋尝试），如果自旋失败（还是没有获取锁）就进入阻塞队列。
 * 这样做的好处是一定程度上避免线程的“阻塞”，“唤醒”带来的cpu开销。
 *
 * <p><p><p><p><p><p><p><p><p><p>
 * 轻量级锁
 * 多线程执行时会有几种情况：1有锁竞争的情况。2很少锁竞争，大部分情况下线程之间获取锁正好是交替的。3几乎不会有锁竞争。
 * 轻量级锁目的：为了应对线程之间交替获取锁的场景。上面讲到重量级锁的获得和释放会消耗大量cpu性能，所以无实际锁竞争的情况下需要有新方式处理。
 * <p>
 * 轻量级锁加锁过程：
 * <a href="https://gorden5566.com/post/1019.html">...</a>
 * 1.Jvm会在线程栈帧中创建一个Lock Record（锁记录（一块区域））
 * 2.拷贝“锁对象”的Mark Word复制到锁记录中
 * 3.Jvm尝试使用CAS（Compare and swap比较并交换）（比较两个Mark Word状态）操作将“锁对象”的Mark Word更新为一个指针（指针指向Lock Record），如果更新成功则获取锁。如果更新失败锁会膨胀为重量级锁。
 * <p>
 * 锁重入的情况
 * 一个线程获取了锁后又再次执行获取锁的操作。这样的话每次执行获取锁的动作，线程都会复制“锁对象”的Mark Word，内部会有多个“锁记录”，每个锁记录都会进行一次CAS操作，本来进行一次CAS操作足矣，这样就影响了性能。
 *
 * <p><p><p><p><p><p><p><p><p><p>
 * 轻量级锁的偏向优化
 * 本质: 是对CAS次数的优化。
 * 第一次执行CAS操作的时候将线程ID设置到对象的Mark Word，只要Mark Word是当前线程ID后面就不会进行CAS操作。这时Mark Word状态就是偏向状态。
 * 当一个线程cpu时间片执行完，发生了线程切换（无锁竞争的线程切换），偏向锁就会升级为轻量级锁。
 * <p>
 * 批量重偏向
 * 重偏向：假如有两个线程A和B，在执行时发生了线程切换导致原先“锁对象”是偏向线程A的现在却偏向了线程B，这就是发生了重偏向。
 * 至于批量重偏向就不做解释了，打字太累。
 *
 */
public class HeimaThreadCreate02 {

    private static final Object lock = new Object();

    public static void main(String[] args) {
        // synchronized就是利用lock的MarkWord关联一个Monitor
        synchronized (lock) {
            System.out.println("hello world");
        }
    }

    // 轻量级锁
    public static void t1() {
        synchronized (lock) {
            // 发生锁重入，线程内部会有多个Lock Record
            t2();
        }
    }

    public static void t2() {
        synchronized (lock) {
            t3();
        }
    }

    public static void t3() {
        synchronized (lock) {
            System.out.println("hello world");
        }
    }
}
