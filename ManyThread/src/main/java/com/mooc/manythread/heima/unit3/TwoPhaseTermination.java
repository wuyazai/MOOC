package com.mooc.manythread.heima.unit3;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;


/**
 * 假如有一个处理任务的线程，我们有时候想使用它，有时候想停止它。我们如何做到优雅停止呢？
 * 使用"两阶段终止模式"
 */
@Slf4j
class TwoPhaseTermination {

    private Thread monitor;

    public void start() {
        monitor = new Thread(() -> {
            while (true) {
                log.debug("执行各种任务。。。");

                Thread curTread = Thread.currentThread();
                if (curTread.isInterrupted()) {
                    break;
                }

                try {
                    // 每隔两秒执行一次任务
                    TimeUnit.SECONDS.sleep(2);
                    // sleep join wait 方法执行期间 interrupt()方法的打断标记会是false需要重新打断
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    curTread.interrupt(); // 重新打断
                }
            }
        });
        monitor.start();
    }

    public void stop() {
        // 打断monitor线程，1有可能是在monitor睡眠时打断，2有可能是在monitor运行时打断
        monitor.interrupt();
    }

}
