package com.mooc.manythread.heima.unit2;

import lombok.extern.slf4j.Slf4j;

/**
 * 03 线程常见的方法 以及线程状态
 */
@Slf4j
public class HeimaThreadCreate04 {

    private static final Object o = new Object();

    public static void main(String[] args) throws InterruptedException {

        // TimeUnit.SECONDS.sleep()方法 会使线程从Running进入TIMED_WAITING状态
        // sleep()方法不要写在同步块中 他不会释放锁
//        log.debug("开始");
//        TimeUnit.SECONDS.sleep(2);
//        System.out.println("hello world");
//        log.debug("结束");


        /*
         * 线程方法 getState() 获取线程状态
         */
//        Thread thread = new Thread(() -> {
//            System.out.println("hello world");
//        }, "thread H");
//        System.out.println(thread.getState());
//        thread.start();
//        System.out.println(thread.getState());


        /*
         * 线程方法 setPriority() 用于设置更改线程的优先级
         */
//        Runnable task1 = () -> {
//            int count = 0;
//            for (; ; ) {
//                System.out.println("---->1 " + count++);
//            }
//        };
//        Runnable task2 = () -> {
//            int count = 0;
//            for (; ; ) {
//                System.out.println("        ---->2 " + count++);
//            }
//        };
//        Thread t1 = new Thread(task1, "t1");
//        Thread t2 = new Thread(task2, "t2");
//        t1.setPriority(Thread.MIN_PRIORITY); // 最高优先级
//        t2.setPriority(Thread.MAX_PRIORITY); // 最低优先级
//        t1.start();
//        t2.start();


        /*
         * 线程方法 interrupt() 打断。打断阻塞状态的线程会以异常形式暴露，且打断标记仍为false。如果是正常running状态的线程被打断，打断标记会为true。isInterrupted()
         * interrupt() 它基于「一个线程不应该由其他线程来强制中断或停止，而是应该由线程自己自行停止。」思想，是一个比较温柔的做法。
         * interrupt() 并不能真正的中断线程，这点要谨记。需要被调用的线程自己进行配合才行。
         *
         * stop()
         * suspend()
         * resume()
         * 这三个方法不推荐使用，不会释放锁，破坏同步代码块，造成线程死锁。在多线程编程中interrupt()方法可以优雅的停止线程。
         */
//        Thread t1 = new Thread(() -> {
//            try {
//                TimeUnit.SECONDS.sleep(3);
//            } catch (InterruptedException e) {
//                log.debug("被叫醒...");
//                e.printStackTrace();
//            }
//            System.out.println("hello world");
//        }, "thread t1");
//        t1.start();
//        TimeUnit.SECONDS.sleep(1);
//        System.out.println(t1.getState());
//        t1.interrupt();


        /*
         * Thread.yield() 让步。 会使线程从Running进入Runnable就绪状态 线程仍然可能会被分配cpu时间片
         */
//        Thread t1 = new Thread(() -> {
//            try {
//                Thread.yield();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            System.out.println("hello world");
//        }, "thread t1");
//        t1.start();
//        TimeUnit.SECONDS.sleep(1);
//        System.out.println(t1.getState());


        /*
         * 线程方法 join() 等待线程执行结束。线程同步。
         *
         */
//        t1();


        /*
         * 锁对象方法 wait()/notify() 等待/通知。使用wait()方法的线程会进入WaitSet等待池，线程状态Waiting状态
         * Monitor有三个重要字段 WaitSet，EntryList，Owner。
         * 获取锁才能wait，获取锁才能notify
         */
//        Thread t1 = new Thread(() -> {
//            synchronized (o) {
//                try {
//                    o.wait();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                log.debug("i week up");
//            }
//        }, "thread t1");
//        t1.start();
//        TimeUnit.SECONDS.sleep(1); //不睡一秒会卡死
//        synchronized (o) {
//            log.debug("通知-------------");
//            o.notify();
//        }


        /*
         * LockSupport.park() 线程休眠(阻塞)工具类，jdk中较为底层的类，AQS中有广泛使用。
         * 但如果一个线程中有多处LockSupport.park()休眠，执行一次interrupt()方法后其他的LockSupport.park()将不再使线程休眠。
         */
//        Thread thread = new Thread(() -> {
//            log.debug("park...");
//            LockSupport.park();
//            log.debug("unPark...");
//            log.debug("打断状态，{}", Thread.currentThread().isInterrupted());
//            // LockSupport.park();
//            // log.debug("我没有休眠...");
//        });
//        thread.start();
//        TimeUnit.SECONDS.sleep(2);
//        thread.interrupt();


        /*
         * 守护线程：其他非守护线程执行结束，它会立即结束。垃圾回收线程就是守护线程。
         */
//        Thread thread = new Thread(() -> {
//            while (true) {
//                if (Thread.currentThread().isInterrupted()) {
//                    break;
//                }
//            }
//        });
//        thread.setDaemon(true); //设置线程为守护线程
//        thread.start();
//
//        TimeUnit.SECONDS.sleep(2);
////        thread.interrupt();
//        log.debug("main线程结束");

    }


    /*
     * 线程方法 join() 等待线程执行结束
     * thread1线程睡眠一秒才会计算r=10，而主线程一开始就要打印r结果
     */
//    static int r = 0;
//
//    private static void t1() throws InterruptedException {
//        log.debug("开始");
//        Thread thread1 = new Thread(() -> {
//            log.debug("开始");
//            try {
//                TimeUnit.SECONDS.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            log.debug("结束");
//            r = 10;
//        }, "thread1");
//        thread1.start();
//        // 等待thread1线程执行结束
//        thread1.join();
//        log.debug("结果为：{}", r);
//        log.debug("结束");
//    }

}
