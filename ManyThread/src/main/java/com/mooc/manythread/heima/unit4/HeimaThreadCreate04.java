package com.mooc.manythread.heima.unit4;

import com.mooc.manythread.common.Downloader;
import lombok.extern.slf4j.Slf4j;

/**
 * 多线程设计模式--保护性暂停模式
 * <p>
 * 锁对象方法 wait()/notify() 之 保护性暂停模式
 * 线程方法 join() 也是使用这个模式来实现的,但他是等待另一个线程执行结束
 * 这个设计模式比join方法好,用在一个线程等待另一个线程的执行结果时
 * <p>
 * 如果有结果不断地从一个线程到另一个线程那么可以使用消息队列（见 生产者/消费者 模式）。
 */
@Slf4j
public class HeimaThreadCreate04 {

    public static void main(String[] args) {
        GuardedObject guardedObject = new GuardedObject();
        new Thread(() -> {
            // 等待结果
            log.debug("等待结果");
            String result = (String) guardedObject.get(3000); // 最大等待时间
            log.debug("结果: {}", result);
        }, "t1").start();

        new Thread(() -> {
            log.debug("执行下载");
            guardedObject.complete(Downloader.download());
        }, "t2").start();

    }

}


/*
 * 执行结果类
 * 两个线程共用一个结果类
 */
//class GuardedObject {
//
//    // 结果
//    private Object response;
//
//    // 获取结果
//    public Object get() {
//        synchronized (this) {
//            // 没有结果
//            while (null == response) { // 这里用While是为了防止虚假唤醒。
//                try {
//                    this.wait();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//            return response;
//        }
//    }
//
//    // 产生结果
//    public void complete(Object response) {
//        synchronized (this) {
//            // 给结果成员变量赋值
//            this.response = response;
//            this.notifyAll();
//        }
//    }
//
//}


/**
 * 优化：超过最大等待时间返回null值
 */
class GuardedObject {

    // 结果
    private Object response;

    /**
     * 获取结果
     *
     * @param maximumLatency 最大等待时间 毫秒
     * @return 返回值
     */
    public Object get(long maximumLatency) {
        synchronized (this) {
            long begin = System.currentTimeMillis(); // 开始时间

            long waitedTime = 0; // 已等待时间

            while (null == response) { // 这里用While是为了防止虚假唤醒。当complete()方法给的值是null时,会造成一直等待的问题。
                if (waitedTime >= maximumLatency) {
                    break;
                }
                try {
                    this.wait(maximumLatency - waitedTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                waitedTime = System.currentTimeMillis() - begin;
            }

            return response;
        }
    }

    // 产生结果
    public void complete(Object response) {
        synchronized (this) {
            // 给结果成员变量赋值
            this.response = response;
            this.notifyAll();
        }
    }

}