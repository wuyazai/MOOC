package com.mooc.manythread.heima.unit4;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * 锁对象方法 wait()/notify() 机制的使用。这两个是Object的方法。
 * <p>
 * Monitor有三个重要字段 WaitSet，EntryList，Owner。
 * 使用wait()方法的线程会进入WaitSet等待池，线程状态Waiting状态。
 * 获取锁才能wait，获取锁才能notify。
 */
@Slf4j
public class HeimaThreadCreate03 {

    static final Object room = new Object();
    static boolean hasCigarette = false; // 有没有烟
    static boolean hasTakeout = false;

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            synchronized (room) {
                log.debug("有烟没有？[{}]", hasCigarette);
                if (!hasCigarette) {
                    log.debug("没烟.先歇会！");
                    try {
                        room.wait();
                        // sleep方法是不会释放锁的，其他线程获取不到锁
                        // TimeUnit.SECONDS.sleep(3);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                log.debug("有烟没有？[{}]", hasCigarette);
                if (hasCigarette) {
                    log.debug("南南开始干活了");
                }
            }
        }, "小南").start();

        for (int i = 0; i < 5; i++) {
            new Thread(() -> {
                synchronized (room) {
                    log.debug("可以开始干活了");
                }
            }, "其他人").start();
        }

        TimeUnit.SECONDS.sleep(1);
        new Thread(() -> {
            synchronized (room) {
                hasCigarette = true;
                log.debug("烟到了！");
                room.notify();
            }
        }, "送烟的").start();

    }


}
