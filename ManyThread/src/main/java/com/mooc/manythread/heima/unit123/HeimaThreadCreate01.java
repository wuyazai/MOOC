package com.mooc.manythread.heima.unit123;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 01创建线程
 * 总结：
 * Callable Runnable是等价的。 区别：一个有返回值一个没有。
 * <p>
 * Future：<a href="https://blog.csdn.net/f641385712/article/details/83546803">...</a>
 *      接口提供了中断任务，判断任务是否中断，获取任务返回值的方法。我们可以用Future接口来实现多线程中的异步调用，即利用等待时间处理其他任务。
 *      FutureTask：Future接口的实现。
 */
@Slf4j
public class HeimaThreadCreate01 {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
//        Thread thread = new Thread("thread01") {
//            public void run() {
//                log.debug("hello");
//            }
//        };
//        thread.start();
//        log.debug("hello");


//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                log.debug("hello");
//            }
//        };
//        Thread thread = new Thread(runnable,"thread02");
//        thread.start();
//        log.debug("hello");


//        // FutureTask 也是一个任务对象 但它提供了用来操作任务的函数 其有参构造需传入callable的实现
//        FutureTask<String> task = new FutureTask<>(() -> {
//            log.debug("hello");
//            return "hello";
//        });
//        Thread thread03 = new Thread(task, "thread03");
//        thread03.start();
//        // 主线程等待task任务执行完成
//        String s = task.get();
//        log.debug(s);


        /*
         * CompletableFuture使用了默认线程池ForkJoinPool.commonPool
         * Future对于结果的获取,不是很友好,只能通过阻塞或者轮询的方式得到任务的结果
         * CompletableFuture 提供了一种观察者模式类似的机制,可以让任务执行完成后通知监听的一方
         *
         * supplyAsync() 支持有返回值
         * runAsync() 无返回值
         */
//        CompletableFuture<String> hello = CompletableFuture.supplyAsync(() -> {
//            log.debug("hello");
//            return "hello";
//        });
//        CompletableFuture<Void> helloVoid = CompletableFuture.runAsync(() -> {
//            log.debug("hello");
//        });
//        log.debug("{}", hello.get());

    }


}
