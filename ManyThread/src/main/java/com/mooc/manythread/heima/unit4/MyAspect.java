package com.mooc.manythread.heima.unit4;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * Aop的线程不安全
 */
@Aspect
@Component
class MyAspect {

    private Long start = 0L;

    @Before("")
    public void before() {
        start = System.nanoTime();
    }

    @After("")
    public void after() {
        long end = System.nanoTime();
        // 这个类明显是线程不安全的，线程a执行到这一步的时，有其他线程把start值改动了。
        // 可以使用环绕通知来实现，并把start作为局部变量。
        System.out.println("cost time:" + (end - start));
    }

}
