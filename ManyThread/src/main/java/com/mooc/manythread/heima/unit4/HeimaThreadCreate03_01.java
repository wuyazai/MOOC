package com.mooc.manythread.heima.unit4;

import lombok.extern.slf4j.Slf4j;

/**
 * 线程A 线程B
 * A打印a B打印b
 * AB交替打印各50次
 * wait() notify()
 */

@Slf4j
public class HeimaThreadCreate03_01 {

    static final Object room = new Object();
    static boolean hasCigarette = true;

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            synchronized (room) {
                int index = 0;
                while (index <= 49) {
                    if (!hasCigarette) {
                        try {
                            room.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println("a-" + index);
                    hasCigarette = false;
                    index++;
                    room.notify();
                }
            }
        }, "A").start();


        new Thread(() -> {
            synchronized (room) {
                int index = 0;
                while (index <= 49) {
                    if (hasCigarette) {
                        try {
                            room.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println("b-" + index);
                    hasCigarette = true;
                    index++;
                    room.notify();
                }
            }
        }, "B").start();

    }

}
