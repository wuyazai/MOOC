package com.mooc.manythread.heima.unit123;


/**
 * 05 线程状态
 */
public class HeimaThreadCreate05 {

    /*
     * 线程状态：
     *  操作系统层面有5中
     *    新建状态
     *    就绪状态
     *    运行状态
     *    阻塞状态
     *    结束状态
     *
     *  Java层面有6中
     *    NEW 新建(初始)状态
     *    RUNNABLE 就绪(可运行)状态
     *    BLOCKED 阻塞状态
     *    WAITING 等待状态join (阻塞状态)
     *    TIMED_WAITING 计时等待状态sleep (阻塞状态)
     *    TERMINATED 结束状态
     */
    public static void main(String[] args) {
        
    }

}
