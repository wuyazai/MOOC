package com.mooc.manythread.heima.unit123;

/**
 * Java的运行时数据区与Java内存模型，线程的内存模型
 * 02 栈与栈帧，线程的运行原理
 *    线程的切换
 */
public class HeimaThreadCreate02 {

    public static void main(String[] args) {

    }

    /*
     * 总结：何时会触发线程切换
     *  1 线程的cpu时间片用完
     *  2 垃圾回收
     *  3 有更高优先级的线程需要运行
     *  4 线程自己调用sleep，yield，wait，join，park，synchronized，lock等方法
     *
     * 注意：
     *  Java中的线程是原生的操作系统线程。Java线程:操作系统线程是1:1的模型。
     *  线程这种资源和操作系统的调度器息息相关。
     */

}
