package com.mooc.manythread.heima.unit3;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * 04 多线程设计模式
 * 1.两阶段终止模式.利用interrupt()方法优雅停止线程。
 *
 * <p/>
 *
 * stop()
 * suspend()
 * resume()
 * 这三个方法停止线程过于暴力，不会释放锁。
 */
@Slf4j
public class HeimaThreadCreate05 {

    public static void main(String[] args) throws InterruptedException {
        TwoPhaseTermination twoPhaseTermination = new TwoPhaseTermination();
        twoPhaseTermination.start();
        TimeUnit.SECONDS.sleep(2);
        twoPhaseTermination.stop();
    }

}



