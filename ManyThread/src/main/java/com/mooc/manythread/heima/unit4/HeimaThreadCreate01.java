package com.mooc.manythread.heima.unit4;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * 第四章
 * Java并发之共享模型（由于资源共享造成的线程安全问题）
 * synchronized关键字使用
 * <p>
 * 判断代码是否线程安全是门艺术活儿
 * <p>
 */
public class HeimaThreadCreate01 {

    /**
     * 多线程操作同一资源所带来的安全问题，synchronized可以保证“可见性”，“有序性”，“原子性”
     * synchronized (对象) 多个线程必须获取同一个对象锁
     * {
     * 临界区
     * }
     * synchronized 实际上是利用“对象锁”来保证临界区域内代码的原子性。
     * 在静态方法上使用synchronized是“类锁(字节码对象锁)”
     * 在非静态方法上使用synchronized是“对象锁(对象实例锁)”
     */

    static int counter = 0;
    static final Object o = new Object();

    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 50000; i++) {
                synchronized (o) {
                    counter++;
                }
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 50000; i++) {
                synchronized (o) {
                    counter--;
                }
            }
        });

        t1.start();
        t2.start();
        System.out.println(counter);
    }

}



