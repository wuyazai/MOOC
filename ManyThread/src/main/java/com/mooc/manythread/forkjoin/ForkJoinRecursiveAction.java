package com.mooc.manythread.forkjoin;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

/**
 * @author likun
 */
public class ForkJoinRecursiveAction {
    private final static int MAX_THRESHOLD = 100;
    private final static AtomicInteger SUM = new AtomicInteger(0);

    private static class CalculateRecursiveAction extends RecursiveAction {
        private final int start;
        private final int end;

        private CalculateRecursiveAction(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        protected void compute() {
            if (this.end - this.start <= ForkJoinRecursiveAction.MAX_THRESHOLD) {
                ForkJoinRecursiveAction.SUM.addAndGet(IntStream.rangeClosed(this.start, this.end).sum());
            } else {
                int middle = (end - start) / 2;
                CalculateRecursiveAction leftAction = new CalculateRecursiveAction(this.start, middle);
                CalculateRecursiveAction rightAction = new CalculateRecursiveAction(middle + 1, this.end);
                leftAction.fork();
                rightAction.fork();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ForkJoinPool pool = new ForkJoinPool();
        pool.submit(new CalculateRecursiveAction(1, 100000000));

        boolean b = pool.awaitTermination(10, TimeUnit.SECONDS);
        System.out.println(SUM);
    }
}

