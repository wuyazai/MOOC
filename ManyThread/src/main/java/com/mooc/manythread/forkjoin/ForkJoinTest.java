package com.mooc.manythread.forkjoin;

import java.util.concurrent.ForkJoinPool;

/**
 * <p>
 *  ForkJoin 线程池
 * </p>
 *
 * @author lk
 * @date 2021/10/20 0:23
 */
public class ForkJoinTest {

    /**
     * 官方建议: 使用预定义的公共线程池 * <a href="https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ForkJoinPool.html">...</a>
     *
     */
    public static void main(String[] args) {
        ForkJoinPool forkJoinPool = ForkJoinPool.commonPool();
    }

}
