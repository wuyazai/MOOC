package com.mooc.manythread.forkjoin;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.IntStream;

/**
 * @author likun
 */
public class ForkJoinRecursiveTask {
    private final static int MAX_THRESHOLD = 100;

    private static class CalculatedRecursiveTask extends RecursiveTask<Integer> {
        private final int start;
        private final int end;

        private CalculatedRecursiveTask(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        protected Integer compute() {
            if (this.end - this.start <= ForkJoinRecursiveTask.MAX_THRESHOLD) {
                return IntStream.rangeClosed(this.start, this.end).sum();
            } else {
                int middle = (start + end) / 2;
                CalculatedRecursiveTask leftTask = new CalculatedRecursiveTask(this.start, middle);
                CalculatedRecursiveTask rightTask = new CalculatedRecursiveTask(middle + 1, this.end);
                leftTask.fork();
                rightTask.fork();
                return leftTask.join() + rightTask.join();
            }
        }
    }

    public static void main(String[] args) {
        final ForkJoinPool pool = new ForkJoinPool();
        ForkJoinTask<Integer> future = pool.submit(new CalculatedRecursiveTask(1, 1000000));
        try {
            Integer ret = future.get();
            System.out.println(ret);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
