package org.example;


import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.ByteBuffer;

/**
 * java nio & netty
 * <a href="https://learn.lianglianglee.com/">...</a>
 */
public class Test {

    /**
     * ByteBuf 是对 ByteBuffer 的封装
     */
    static ByteBuf buf = Unpooled.compositeBuffer();

    public static void main(String[] args) {
        // java nio
        // 通过 DirectByteBuffer 管理堆外内存 这个对象被回收 堆外内存就会释放
        // 具体执行是 Cleaner 类来处理
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(1024);




    }

}
