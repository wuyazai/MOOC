package com.baidu.toolstest;

import java.util.Scanner;

/**
 * <p>
 * 不死神兔
 * </p>
 *
 * @author kk
 * @date 2021/12/2 16:26
 */
public class NotDeathRabbit {

    public static void main(String[] args) {
        // 递归
        int mun = 0;
        Scanner scanner = new Scanner(System.in);
        int mouth = scanner.nextInt();
        mun = rabbit(mouth);
        System.out.println("第" + mouth + "月兔子数量为：" + mun);
    }

    /**
     * 一只兔子,从出生后第三个月起每个月都生一只兔子,小兔子长到第三个月后又生一只兔子,假如兔子都不死,第n个月兔子总数为多少？
     */
    public static Integer rabbit(int mouth) {
        if (mouth > 2) {
            return rabbit(mouth - 1) + rabbit(mouth - 2);
        } else {
            return 1;
        }
    }

}
