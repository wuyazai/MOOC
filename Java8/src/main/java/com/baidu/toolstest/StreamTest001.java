package com.baidu.toolstest;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * createDate: 2021/3/27 20:39
 *
 * @author likun
 */
public class StreamTest001 {

    public static void main(String[] args) {

        // stream流拼接字符串测试
//        ArrayList<String> columnLis = new ArrayList<String>();
//        columnLis.add("1");
//        columnLis.add("2");
//        columnLis.add("3");
//        String columnSQL = columnLis.stream().filter(s -> !s.isEmpty()).collect(Collectors.joining(","));
//        System.out.println(columnSQL);

        // stream 流求和
//        ArrayList<Double> doubles = new ArrayList<>();
//        doubles.add(0.1);
//        doubles.add(1.1);
//        doubles.add(2.1);
//        doubles.add(2.1);
//        doubles.add(3.1);
//        DoubleSummaryStatistics doubleSummaryStatistics = doubles.stream().mapToDouble((s) -> s).summaryStatistics();
//        double sum = doubleSummaryStatistics.getSum();
//        System.out.println(sum);

        // stream流List<Map<>>型 操作map
//        ArrayList<HashMap<String,String>> maps = new ArrayList<>();
//        HashMap<String,String> put1 = new HashMap<String, String>();
//        put1.put("a1", "a4a4a5df4");
//        put1.put("a2", "99");
//        put1.put("a3", "a4ds5d54df4");
//        HashMap<String, String> put2 = new HashMap<String, String>();
//        put2.put("a1", "a4asa4df56adf4");
//        put2.put("a2", "88");
//        put2.put("a3", "a4ds8s8df65a4");
//        maps.add(put1);
//        maps.add(put2);
//        Map<String, String> collect = maps.stream().collect(Collectors.toMap(e -> e.get("a2"), a -> a.get("a3")));
//        String s = JSON.toJSONString(collect);
//        System.out.println(s);

        // Java中的lambda表达式 和 “::”方法引用
//        Function<String, String> fun = (String x) -> x + "爱你:)";
//        System.out.println(fun.apply("我"));

        // 函数式接口使用注解@FunctionalInterface标识,并且只包含一个抽象方法。函数式接口主要分为Supplier供给型函数、Consumer消费型函数、Runnable无参无返回型函数和Function有参有返回型函数(转换型函数)。
//        BiFunction<String, String, String> stuStringStuBiFu = (String name, String blimey) -> name + "此人没有住址,很穷,很屌丝" + blimey;
//        String name = "李坤,";
//        String blimey = ",好苦逼!";
//        String apply = stuStringStuBiFu.apply(name, blimey);
//        System.out.println(apply);

        // Consumer没参,没返回值
//        Consumer<Student> learn = Student::learn;
        // BiConsumer有参,没返回值
//        BiConsumer<Student, String> setAddress = Student::setAddress;
        // Function没参,有返回值
//        Function<Student, String> eat = Student::eat;
        // BiFunction有参,有返回值
//        BiFunction<Student, String, String> exam = Student::exam;

        // 匿名内部类和lambda表达式,两者都运用父类的行为
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("hello unnamed Runnable");
//            }
//        };
//        runnable.run();

//        Thread thread = new Thread() {
//            @Override
//            public void run() {
//                System.out.println("im unnamed thread");
//            }
//        };
//        thread.start();

        // lambda 与函数式接口
//        Runnable runnableLam = () -> System.out.println("hello im Runnable lambda");
//        runnableLam.run();
//        Runnable runnable = () -> {System.out.println("hello im Runnable lambda");};
//        runnable.run();

    }

    /**
     * 某种字体
     * 我使用的是 Ubuntu_Mono 字体
     *
     * @return Font
     */
    private static Font getSelfDefinedFont() {
        String filepath = "C:\\IdeaWorkMy\\MOOC\\Java8\\lib\\Ubuntu_Mono\\UbuntuMono-Regular.ttf";

        Font font = null;
        File file = new File(filepath);
        try {
            font = Font.createFont(Font.TRUETYPE_FONT, file);
            font = font.deriveFont(Font.PLAIN, 40);
        } catch (FontFormatException | IOException e) {
            return null;
        }
        return font;
    }


}

