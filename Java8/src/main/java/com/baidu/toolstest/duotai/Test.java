package com.baidu.toolstest.duotai;


/**
 * 深刻理解多态
 * 多态的设计目的: “类型”
 * <>p</>
 * 比如下面的 People 和 学生 Student
 */
public class Test {


    private String studentNo;
    private String banJi;
    private String nianJi;

    public static void main(String[] args) {
        Student student = new Student();
        student.setStudentNo("123456");
        student.setBanJi("123456");
        student.setNianJi("123456");
        student.setAge("123456");
        student.setSex("123456");
        student.setAddress("123456");
        student.setAddress("中国");

        // people 中拥有 Student对象的所有内容。Student即是people又是student
        System.out.println(student instanceof People);
    }

}
