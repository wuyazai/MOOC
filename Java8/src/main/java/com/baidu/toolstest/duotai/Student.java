package com.baidu.toolstest.duotai;



public class Student extends People {

    private String studentNo;
    private String banJi;
    private String nianJi;

    public Student() {
    }

    public Student(String studentNo, String banJi, String nianJi) {
        this.studentNo = studentNo;
        this.banJi = banJi;
        this.nianJi = nianJi;
    }

    public String getStudentNo() {
        return studentNo;
    }

    public void setStudentNo(String studentNo) {
        this.studentNo = studentNo;
    }

    public String getBanJi() {
        return banJi;
    }

    public void setBanJi(String banJi) {
        this.banJi = banJi;
    }

    public String getNianJi() {
        return nianJi;
    }

    public void setNianJi(String nianJi) {
        this.nianJi = nianJi;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentNo='" + studentNo + '\'' +
                ", banJi='" + banJi + '\'' +
                ", nianJi='" + nianJi + '\'' +
                '}';
    }
}
