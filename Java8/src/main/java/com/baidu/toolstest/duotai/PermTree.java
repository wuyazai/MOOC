package com.baidu.toolstest.duotai;

import java.util.List;

public class PermTree {

    private String id;
    private String parentId;
    private String address;

    private List<PermTree> tree;

    public PermTree() {
    }

    public PermTree(String id, String parentId, String address, List<PermTree> tree) {
        this.id = id;
        this.parentId = parentId;
        this.address = address;
        this.tree = tree;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<PermTree> getTree() {
        return tree;
    }

    public void setTree(List<PermTree> tree) {
        this.tree = tree;
    }

    @Override
    public String toString() {
        return "PermTree{" +
                "id='" + id + '\'' +
                ", parentId='" + parentId + '\'' +
                ", address='" + address + '\'' +
                ", tree=" + tree +
                '}';
    }
}
