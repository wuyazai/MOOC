package com.baidu.toolstest.duotai;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;

public class ArrayListTest {

    public static void main(String[] args) {
        ArrayList<PermTree> objects = new ArrayList<>();
        PermTree permTree = new PermTree();
        permTree.setAddress("古城");
        permTree.setParentId("古城");
        permTree.setId("1");

        PermTree permTree1 = new PermTree();
        permTree1.setAddress("里城");
        permTree1.setParentId("里城");
        permTree1.setId("2");

        objects.add(permTree1);
        objects.add(permTree);

        String s = JSON.toJSONString(objects);
        System.out.println(s);
    }

}
