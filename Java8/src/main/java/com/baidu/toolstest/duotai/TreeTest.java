package com.baidu.toolstest.duotai;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 非递归获取树形结构
 */
public class TreeTest {


    public static void main(String[] args) {
        ArrayList<PermTree> allSource = new ArrayList<>();

        PermTree permTree1 = new PermTree();
        permTree1.setId("1");
        permTree1.setAddress("111111");
        permTree1.setParentId("unms");

        PermTree permTree2 = new PermTree();
        permTree2.setId("2");
        permTree2.setAddress("222222");
        permTree2.setParentId("1");

        PermTree permTree3 = new PermTree();
        permTree3.setId("3");
        permTree3.setAddress("333333");
        permTree3.setParentId("1");

        PermTree permTree4 = new PermTree();
        permTree4.setId("4");
        permTree4.setAddress("444444");
        permTree4.setParentId("2");

        PermTree permTree5 = new PermTree();
        permTree5.setId("5");
        permTree5.setAddress("444444");
        permTree5.setParentId("2");

        PermTree permTree6 = new PermTree();
        permTree6.setId("6");
        permTree6.setAddress("444444");
        permTree6.setParentId("3");

        allSource.add(permTree1);
        allSource.add(permTree2);
        allSource.add(permTree3);
        allSource.add(permTree4);
        allSource.add(permTree5);
        allSource.add(permTree6);

//        List<PermTree> trees = new ArrayList<>();
//        for (PermTree s : allSource) {
//            if (s.getId().equals("1")) {
//                trees.add(s);
//            }
//            for (PermTree p : allSource) {
//                if (s.getId().equals(p.getParentId())) {
//                    List<PermTree> permTrees = Optional.ofNullable(s.getTree()).orElse(new ArrayList<>());
//
//                    permTrees.add(p);
//                    s.setTree(permTrees);
//                }
//            }
//        }
//        String s = JSON.toJSONString(trees);
//        System.out.println(s);



        // parentID PermTree集合
        Map<String, List<PermTree>> collect = allSource.stream().collect(Collectors.groupingBy(PermTree::getParentId));
        for (PermTree permTree : allSource) {
            permTree.setTree(collect.get(permTree.getId()));
        }
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            System.out.println(allSource.toString());
            String s = objectMapper.writeValueAsString(allSource);
            System.out.println(s);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }



//        // parentID PermTree集合
//        Map<String, List<PermTree>> collect = allSource.stream().collect(Collectors.groupingBy(PermTree::getParentId));
//        // 所有元素的id
//        List<String> ids = allSource.stream().map(PermTree::getId).collect(Collectors.toList());
//        // 所有被引用的id
//        List<String> parentIds = allSource.stream().map(PermTree::getParentId).collect(Collectors.toList());
//        // 所有没有被引用的id
//        List<String> differenceId = ids.stream().filter(id -> !parentIds.contains(id)).collect(Collectors.toList());
//
//        List<PermTree> sourcesNew = allSource.stream().filter(s -> !differenceId.contains(s.getId())).collect(Collectors.toList());
//
//        for (PermTree permTree : sourcesNew) {
//            permTree.setTree(collect.get(permTree.getId()));
//        }
//        ObjectMapper objectMapper = new ObjectMapper();
//        try {
//            System.out.println(sourcesNew.toString());
//            String s = objectMapper.writeValueAsString(sourcesNew);
//            System.out.println(s);
//        } catch (JsonProcessingException e) {
//            throw new RuntimeException(e);
//        }


    }

}
