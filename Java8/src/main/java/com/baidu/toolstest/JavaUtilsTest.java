package com.baidu.toolstest;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * <p>
 * 常用原生库函数测试
 * </p>
 *
 * @author kk
 * @date 2021/12/2 16:33
 */
public class JavaUtilsTest {
    public static void main(String[] args) {
        // stream流 join测试
//        List<String> list = new ArrayList<>();
//        list.add("测试1");
//        list.add("测试2");
//        list.add("测试3");
//        String join = String.join(",", list);
//        System.out.println(join);

        // substring函数测试
//        String s = "www.baidu.com";
//        System.out.println(s.substring(0, 2));

        // BigDecimal函数测试
//        BigDecimal bigDecimal = new BigDecimal(10).negate();
//        System.out.println(bigDecimal);

//        Integer i = 20;
//        Integer j = i++;
//        boolean equals = i.equals(j);
//        System.out.println(equals);

        // 集合初始化大小 会不会扩容问题
//        ArrayList<Object> objects = new ArrayList<>(5);
//        for (int m = 0; m < 10; m++) {
//            objects.add(m);
//        }
//        System.out.println(objects);

        // indexOf 函数
//        String l = "456123";
//        String m = "123";
//        int ii = l.indexOf(m);
//        System.out.println(ii);


//        RSA rsa = new RSA();
//        //获得私钥
//        PrivateKey privateKey = rsa.getPrivateKey();
//
//        //获得公钥
//        PublicKey publicKey = rsa.getPublicKey();
//
//        //公钥加密，私钥解密
//        byte[] encrypt = rsa.encrypt(StrUtil.bytes("我是一段测试", CharsetUtil.CHARSET_UTF_8), KeyType.PublicKey);
//
//        byte[] decrypt = rsa.decrypt(encrypt, KeyType.PrivateKey);
//
//        System.out.println(new String(decrypt));

//        KeyPair pair = SecureUtil.generateKeyPair("RSA");
//        PrivateKey aPrivate = pair.getPrivate();
//        PublicKey aPublic = pair.getPublic();
    }
}
