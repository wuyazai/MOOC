package com.baidu.scalatest;

/**
 * <p>
 * 列对齐
 * </p>
 *
 * @author kk
 */
object FontTest {
  def main(args: Array[String]): Unit = {
    printf("%-8s\t%-8s\t%-8s\t%-8s\n", "姓名", "年龄", "籍贯", "职业");
    printf("%-8s\t%-8s\t%-8s\t%-8s\n", "张三", "21", "上海", "快递员");
    printf("%-8s\t%-8s\t%-8s\t%-8s\n", "李四", "26", "乌鲁木齐", "程序员");
  }
}


