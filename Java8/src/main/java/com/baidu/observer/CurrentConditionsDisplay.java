package com.baidu.observer;

import java.util.Observable;
import java.util.Observer;

// 观察者
public class CurrentConditionsDisplay implements Observer {
    private float temperature;
    private float humidity;

    public void update(Observable obs, Object arg) {
        if (obs instanceof WeatherStationObservable) {
            WeatherStationObservable weather = (WeatherStationObservable) obs;
            this.temperature = weather.getTemperature();
            this.humidity = weather.getHumidity();
            display();
        }
    }

    public void display() {
        System.out.println("Current conditions: " + temperature + "F degrees and " + humidity + "% humidity");
    }
}


// 定义另一个观察者 - 统计数据显示
class StatisticsDisplay implements Observer {
    private float maxTemp = 0.0f;
    private float minTemp = 200;
    private float tempSum= 0.0f;
    private int numReadings;

    public void update(Observable obs, Object arg) {
        if (obs instanceof WeatherStationObservable) {
            WeatherStationObservable weather = (WeatherStationObservable) obs;
            float temp = weather.getTemperature();
            tempSum += temp;
            numReadings++;

            if (temp > maxTemp) {
                maxTemp = temp;
            }

            if (temp < minTemp) {
                minTemp = temp;
            }

            display();
        }
    }

    public void display() {
        System.out.println("Avg/Max/Min temperature = " + (tempSum / numReadings)
                + "/" + maxTemp + "/" + minTemp);
    }
}


// 主题 - 天气站 - 被观察者
class WeatherStationObservable extends Observable {
    private float temperature;
    private float humidity;

    public void setMeasurements(float temperature, float humidity) {
        this.temperature = temperature;
        this.humidity = humidity;
        setChanged();
        notifyObservers();
    }

    public float getTemperature() {
        return temperature;
    }

    public float getHumidity() {
        return humidity;
    }
}


// 测试程序
class WeatherStation {
    public static void main(String[] args) {
        WeatherStationObservable weatherData = new WeatherStationObservable();

        CurrentConditionsDisplay currentDisplay = new CurrentConditionsDisplay();
        StatisticsDisplay statisticsDisplay = new StatisticsDisplay();

        // Observable 维护 Observer 列表
        weatherData.addObserver(currentDisplay);
        weatherData.addObserver(statisticsDisplay);

        // 模拟天气数据更新
        weatherData.setMeasurements(80, 65);
        weatherData.setMeasurements(82, 70);
        weatherData.setMeasurements(78, 90);
    }
}

