package com.baidu.controller;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ConfigAuto {

    @Bean
    public RestTemplate getHttpClient() {
        return new RestTemplate();
    }


}
