package com.baidu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RequestMapping("/oauth/github/")
@RestController
public class OauthGithubController {

    @Autowired
    private RestTemplate restTemplate;

    /**
     * GitHub是“服务提供商”
     * 此后端服务是“客户端”
     * <p>
     * Oauth2.0授权码模式流程
     * 1.前端请求如下接口：                                  GitHub给的客户端ID               在GitHub配置的回调接口
     * <a href="https://github.com/login/oauth/authorize?client_id=1f72ceee7845d6434dab&redirect_uri=http://localhost:8082/oauth/github/login">...</a>
     * 2.第一步过后GitHub会携带一个code来调用咱们的这个接口，Github验证后给咱颁发token
     */

    /*
     * 这个是被github重定向的接口
     */
    @GetMapping("/login")
    public Object login(@RequestParam String code) {
        /*
         * client_id 客户端的ID (后端服务是github的客户端)
         * client_secret 客户端的密钥
         * code 授权码
         * client_id,client_secret都是在https://github.com/settings/applications/new设置好的
         */
        String url = "https://github.com/login/oauth/access_token" + "?"
                + "client_id=1f72ceee7845d6434dab" + "&"
                + "client_secret=095b7d48311f78e9d779bc470115b8c8bb8db710" + "&"
                + "code=" + code;
        RestTemplate restTemplate = new RestTemplate();

        /*
         * 返回GitHub颁发的token
         *
         * {
         * 	"access_token": "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",
         * 	"token_type": "bearer",
         * 	"scope": ""
         * }
         *
         * 随后我们可以通过token去https://api.github.com/user获取用户信息
         */
        return restTemplate.postForObject(url, null, Object.class);
    }

}
