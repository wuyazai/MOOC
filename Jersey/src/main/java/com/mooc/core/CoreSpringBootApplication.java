package com.mooc.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author likun
 */
@SpringBootApplication
public class CoreSpringBootApplication {

    private final Logger logger = LoggerFactory.getLogger(CoreSpringBootApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(CoreSpringBootApplication.class, args);
    }


}
