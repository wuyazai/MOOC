package com.mooc.core.webservicefilter;

import java.lang.reflect.Method;
import java.util.List;
import java.util.StringTokenizer;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

// import org.glassfish.jersey.internal.util.Base64; 这个包找不见改用Java8的解码
import java.util.Base64;

/**
 * This filter verify the access permissions for a user
 * based on username and password provided in request
 *
 * @author likun
 */
@Provider
public class AuthenticationFilter implements javax.ws.rs.container.ContainerRequestFilter {

    @Context
    private ResourceInfo resourceInfo;

    private static final String AUTHORIZATION_PROPERTY = "Authorization";
    private static final String AUTHENTICATION_SCHEME;

    static {
        AUTHENTICATION_SCHEME = "Basic";
    }

    @Override
    public void filter(ContainerRequestContext requestContext) {

        Method method = resourceInfo.getResourceMethod();

        // Access allowed for all
        // PermitAll注解
        // 没有配置PermitAll注解。 方法 method.isAnnotationPresent(PermitAll.class)返回的是false。所以就允许所有人访问:)
        if (!method.isAnnotationPresent(PermitAll.class)) {
            //Access denied for all
            if (method.isAnnotationPresent(DenyAll.class)) {
                requestContext.abortWith(Response.status(Response.Status.FORBIDDEN)
                        .entity("Access blocked for all users !!").build());
                return;
            }

            // Get request headers
            final MultivaluedMap<String, String> headers = requestContext.getHeaders();

            // Fetch authorization header
            final List<String> authorization = headers.get(AUTHORIZATION_PROPERTY);

            // If no authorization information present; block access
            // 如果没有账户+密码
            if (authorization == null || authorization.isEmpty()) {
                requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
                        .entity("You cannot access this resource").build());
                return;
            }

            // Get encoded username and password
            // 获取用户名和密码
            final String encodedUserPassword = authorization.get(0).replaceFirst(AUTHENTICATION_SCHEME + " ", "");

            // Decode username and password
            // 解码获取明文
            Base64.Decoder decoder = Base64.getDecoder();
            String usernameAndPassword = new String(decoder.decode(encodedUserPassword.getBytes()));

            // Split username and password tokens
            // 截取用户名和密码
            final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
            final String username = tokenizer.nextToken();
            final String password = tokenizer.nextToken();

            //Verifying Username and password
//            System.out.println(username);
//            System.out.println(password);

            //Is user valid?
            if (!isUserAllowed(username, password)) {
                //验证账户密码 如果不对就直接响应
                requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity("You cannot access this resource").build());
            }

            //Verify user access
            //可以通过RolesAllowed注解进行角色权限配置,但用处不大,Http BasicAuth 本身就不太安全。
//            if (method.isAnnotationPresent(RolesAllowed.class)) {
//                RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
//                Set<String> rolesSet = new HashSet<String>(Arrays.asList(rolesAnnotation.value()));
//
//                //Is user valid?
//                if (!isUserAllowed(username, password, rolesSet)) {
//                    //鉴权
//                    requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
//                            .entity("You cannot access this resource").build());
//                }
//            }
        }
    }

    //角色鉴权
//    private boolean isUserAllowed(final String username, final String password, final Set<String> rolesSet) {
//        boolean isAllowed = false;
//
//        //Step 1. Fetch password from database and match with password in argument
//        //If both match then get the defined role for user from database and continue; else return isAllowed [false]
//        //Access the database and do this part yourself
//        //String userRole = userMgr.getUserRole(username);
//
//        if (username.equals("username") && password.equals("password")) {
//            String userRole = "ADMIN";
//
//            //Step 2. Verify user role
//            if (rolesSet.contains(userRole)) {
//                isAllowed = true;
//            }
//        }
//        return isAllowed;
//    }

    private boolean isUserAllowed(final String username, final String password) {
        //Step 1. Fetch password from database and match with password in argument
        //If both match then get the defined role for user from database and continue; else return isAllowed [false]
        //Access the database and do this part yourself

        //String userRole = userMgr.getUserRole(username);

        return "username".equals(username) && "password".equals(password);
    }

}