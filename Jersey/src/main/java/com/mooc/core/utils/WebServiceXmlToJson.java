package com.mooc.core.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author likun
 */
@Component
public class WebServiceXmlToJson {

    /**
     * 解析webservice的返回结果
     *
     * @param xmlStr xml内容
     */
    public static JSONObject xml2Json(String xmlStr) throws DocumentException {
        Document doc = DocumentHelper.parseText(xmlStr);
        JSONObject json = new JSONObject();
        dom4j2Json(doc.getRootElement(), json);
        return json;
    }


    /**
     * xml转json
     *
     */
    public static void dom4j2Json(Element element, JSONObject json) {
        List<Element> chdEl = element.elements();
        for (Element e : chdEl) {
            if (!e.elements().isEmpty()) {
                JSONObject jsonObj = new JSONObject();
                dom4j2Json(e, jsonObj);
                Object o = json.get(e.getName());
                if (o != null) {
                    JSONArray jsona = null;
                    if (o instanceof JSONObject) {
                        JSONObject jsonO = (JSONObject) o;
                        json.remove(e.getName());
                        jsona = new JSONArray();
                        jsona.add(jsonO);
                        jsona.add(jsonObj);
                    }
                    if (o instanceof JSONArray) {
                        jsona = (JSONArray) o;
                        jsona.add(jsonObj);
                    }
                    json.put(e.getName(), jsona);
                } else {
                    if (!jsonObj.isEmpty()) {
                        json.put(e.getName(), jsonObj);
                    }
                }
            } else {
                if (!e.getText().isEmpty()) {
                    json.put(e.getName(), e.getText());
                }
            }
        }
    }


    public static String getParameter() {

        String strSoapParameter;

        strSoapParameter = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:urn=\"urn:sap-com:document:sap:rfc:functions\">\n" +
                "   <soap:Header/>\n" +
                "   <soap:Body>\n" +
                "      <urn:ZFLOWFU0001>\n" +
                "         <!--Optional:-->\n" +
                "         <ET_RETURN>\n" +
                "            <!--Zero or more repetitions:-->\n" +
                "            <item>\n" +
                "               <TYPE>?</TYPE>\n" +
                "               <ID>?</ID>\n" +
                "               <NUMBER>?</NUMBER>\n" +
                "               <MESSAGE>?</MESSAGE>\n" +
                "               <LOG_NO>?</LOG_NO>\n" +
                "               <LOG_MSG_NO>?</LOG_MSG_NO>\n" +
                "               <MESSAGE_V1>?</MESSAGE_V1>\n" +
                "               <MESSAGE_V2>?</MESSAGE_V2>\n" +
                "               <MESSAGE_V3>?</MESSAGE_V3>\n" +
                "               <MESSAGE_V4>?</MESSAGE_V4>\n" +
                "               <PARAMETER>?</PARAMETER>\n" +
                "               <ROW>?</ROW>\n" +
                "               <FIELD>?</FIELD>\n" +
                "               <SYSTEM>?</SYSTEM>\n" +
                "            </item>\n" +
                "         </ET_RETURN>\n" +
                "         <IS_HEAD>\n" +
                "            <MANDT>?</MANDT>\n" +
                "            <ID>?</ID>\n" +
                "            <DJ_TYPE>?</DJ_TYPE>\n" +
                "            <ZAPPROVE_FLG>?</ZAPPROVE_FLG>\n" +
                "            <ZSP>?</ZSP>\n" +
                "            <ZGZ>?</ZGZ>\n" +
                "            <BKTXT>?</BKTXT>\n" +
                "            <ERDAT>?</ERDAT>\n" +
                "            <ERZET>?</ERZET>\n" +
                "            <ERNAM>?</ERNAM>\n" +
                "            <REFID>?</REFID>\n" +
                "            <ZJSDJ>?</ZJSDJ>\n" +
                "            <ZFY>?</ZFY>\n" +
                "            <PI_STATUS_FY>?</PI_STATUS_FY>\n" +
                "            <ZSOURCE>?</ZSOURCE>\n" +
                "            <ZSOURCE_ID>?</ZSOURCE_ID>\n" +
                "            <DATPR>?</DATPR>\n" +
                "            <ZSPTXT>?</ZSPTXT>\n" +
                "            <PRINT_COUNT>?</PRINT_COUNT>\n" +
                "            <AUTO_LOCATION>?</AUTO_LOCATION>\n" +
                "         </IS_HEAD>\n" +
                "         <IS_REQ>\n" +
                "            <REQKEYID>?</REQKEYID>\n" +
                "            <MESSAGEID>cid:763068047417</MESSAGEID>\n" +
                "            <SNDPRN>?</SNDPRN>\n" +
                "            <RCVPRN>?</RCVPRN>\n" +
                "            <REQUSER>?</REQUSER>\n" +
                "            <NOTE1>?</NOTE1>\n" +
                "            <NOTE2>?</NOTE2>\n" +
                "            <NOTE3>?</NOTE3>\n" +
                "         </IS_REQ>\n" +
                "         <IT_DETAIL>\n" +
                "            <!--Zero or more repetitions:-->\n" +
                "            <item>\n" +
                "               <MANDT>?</MANDT>\n" +
                "               <ID>?</ID>\n" +
                "               <CANUM>?</CANUM>\n" +
                "               <XAUTO>?</XAUTO>\n" +
                "               <COWB_FLG_NS>?</COWB_FLG_NS>\n" +
                "               <UEPOS>?</UEPOS>\n" +
                "               <REFNO>?</REFNO>\n" +
                "               <BWART>?</BWART>\n" +
                "               <MATNR>?</MATNR>\n" +
                "               <SPART>?</SPART>\n" +
                "               <CHARG>?</CHARG>\n" +
                "               <AUFNR>?</AUFNR>\n" +
                "               <RSNUM>?</RSNUM>\n" +
                "               <RSPOS>?</RSPOS>\n" +
                "               <SOBKZ>?</SOBKZ>\n" +
                "               <VBELN>?</VBELN>\n" +
                "               <POSNR>?</POSNR>\n" +
                "               <PS_PSP_PNR>?</PS_PSP_PNR>\n" +
                "               <EBELN>?</EBELN>\n" +
                "               <EBELP>?</EBELP>\n" +
                "               <WERKS>?</WERKS>\n" +
                "               <LGORT>?</LGORT>\n" +
                "               <ZZCGY>?</ZZCGY>\n" +
                "               <LGNUM>?</LGNUM>\n" +
                "               <LGTYP>?</LGTYP>\n" +
                "               <LGPLA>?</LGPLA>\n" +
                "               <UMSOK>?</UMSOK>\n" +
                "               <MAT_KDAUF>?</MAT_KDAUF>\n" +
                "               <MAT_KDPOS>?</MAT_KDPOS>\n" +
                "               <MAT_PSPNR>?</MAT_PSPNR>\n" +
                "               <UMWRK>?</UMWRK>\n" +
                "               <UMLGO>?</UMLGO>\n" +
                "               <ZZUCGY>?</ZZUCGY>\n" +
                "               <UMMAT>?</UMMAT>\n" +
                "               <UMCHA>?</UMCHA>\n" +
                "               <UGNUM>?</UGNUM>\n" +
                "               <UGTYP>?</UGTYP>\n" +
                "               <UGPLA>?</UGPLA>\n" +
                "               <SHKZG>?</SHKZG>\n" +
                "               <MENGE>?</MENGE>\n" +
                "               <BDMNG>?</BDMNG>\n" +
                "               <ENMNG>?</ENMNG>\n" +
                "               <MEINS>?</MEINS>\n" +
                "               <KZATP>?</KZATP>\n" +
                "               <ATPMN>?</ATPMN>\n" +
                "               <KOKRS>?</KOKRS>\n" +
                "               <KOSTL>?</KOSTL>\n" +
                "               <PRUEFLOS>?</PRUEFLOS>\n" +
                "               <ERFME>?</ERFME>\n" +
                "               <XLOEK>?</XLOEK>\n" +
                "               <KZEAR>?</KZEAR>\n" +
                "               <REMARK>?</REMARK>\n" +
                "               <REMARK2>?</REMARK2>\n" +
                "               <LMENGELZ>?</LMENGELZ>\n" +
                "               <LMENGE01>?</LMENGE01>\n" +
                "               <LMENGE02>?</LMENGE02>\n" +
                "               <LMENGE03>?</LMENGE03>\n" +
                "               <LMENGE04>?</LMENGE04>\n" +
                "               <LMENGE05>?</LMENGE05>\n" +
                "               <LMENGE06>?</LMENGE06>\n" +
                "               <LIFNR>?</LIFNR>\n" +
                "               <KUNNR>?</KUNNR>\n" +
                "               <PARBU>?</PARBU>\n" +
                "               <ANLN1>?</ANLN1>\n" +
                "               <BUKRS>?</BUKRS>\n" +
                "               <ESCH_DT>?</ESCH_DT>\n" +
                "               <REFID>?</REFID>\n" +
                "               <REFCANUM>?</REFCANUM>\n" +
                "               <PRORD>?</PRORD>\n" +
                "               <PRO_CANUM>?</PRO_CANUM>\n" +
                "               <LICHA>?</LICHA>\n" +
                "               <ZJYY>?</ZJYY>\n" +
                "               <ZPDJG>?</ZPDJG>\n" +
                "               <ZKDNUM>?</ZKDNUM>\n" +
                "               <INSMK>?</INSMK>\n" +
                "               <GRUND>?</GRUND>\n" +
                "               <VORNR>?</VORNR>\n" +
                "               <LFBNR>?</LFBNR>\n" +
                "               <LFBJA>?</LFBJA>\n" +
                "               <LFPOS>?</LFPOS>\n" +
                "               <ZZZJMNG>?</ZZZJMNG>\n" +
                "               <ZZYMNG>?</ZZYMNG>\n" +
                "               <ZDDID>?</ZDDID>\n" +
                "               <ZXMID>?</ZXMID>\n" +
                "               <ZTEXT>?</ZTEXT>\n" +
                "               <ZZ_DJ>?</ZZ_DJ>\n" +
                "               <ZZCYMG>?</ZZCYMG>\n" +
                "               <BSTRF>?</BSTRF>\n" +
                "               <ZPM>?</ZPM>\n" +
                "               <ZGGXH>?</ZGGXH>\n" +
                "               <MATKL>?</MATKL>\n" +
                "               <ZGH>?</ZGH>\n" +
                "               <ZJCR>?</ZJCR>\n" +
                "               <ZGHR>?</ZGHR>\n" +
                "               <HSDAT>?</HSDAT>\n" +
                "               <VFDAT>?</VFDAT>\n" +
                "               <EQUNR>?</EQUNR>\n" +
                "               <PLNBEZ>?</PLNBEZ>\n" +
                "               <GAMNG>?</GAMNG>\n" +
                "               <GMEIN>?</GMEIN>\n" +
                "               <ARBPL>?</ARBPL>\n" +
                "               <BSTMI>?</BSTMI>\n" +
                "               <VBELN_VL>?</VBELN_VL>\n" +
                "               <POSNR_VL>?</POSNR_VL>\n" +
                "               <VKORG>?</VKORG>\n" +
                "               <VTWEG>?</VTWEG>\n" +
                "               <PSTYV>?</PSTYV>\n" +
                "               <KOSTA>?</KOSTA>\n" +
                "               <TBNUM>?</TBNUM>\n" +
                "               <TBPOS>?</TBPOS>\n" +
                "               <TANUM>?</TANUM>\n" +
                "               <TAPOS>?</TAPOS>\n" +
                "               <JYLGPLA>?</JYLGPLA>\n" +
                "               <SJLGPLA>?</SJLGPLA>\n" +
                "               <MARK>?</MARK>\n" +
                "               <MAKTX>?</MAKTX>\n" +
                "               <LGOBE>?</LGOBE>\n" +
                "               <UMAKT>?</UMAKT>\n" +
                "               <UGOBE>?</UGOBE>\n" +
                "               <KTEXT>?</KTEXT>\n" +
                "               <JYCHARG>?</JYCHARG>\n" +
                "               <TRART>?</TRART>\n" +
                "               <FEVOR>?</FEVOR>\n" +
                "               <TYPES>?</TYPES>\n" +
                "               <WMSBS>?</WMSBS>\n" +
                "               <SMMNG>?</SMMNG>\n" +
                "               <SMMNG_LAST>?</SMMNG_LAST>\n" +
                "               <ZWQSL>?</ZWQSL>\n" +
                "               <ATEXT>?</ATEXT>\n" +
                "               <F4BUTTON>?</F4BUTTON>\n" +
                "               <LINE_ID>?</LINE_ID>\n" +
                "               <SGTXT>?</SGTXT>\n" +
                "               <SATNR>?</SATNR>\n" +
                "               <BUTTON_BLYY>?</BUTTON_BLYY>\n" +
                "               <MAX_MENGE>?</MAX_MENGE>\n" +
                "               <WERKS_KUNNR>?</WERKS_KUNNR>\n" +
                "               <ZZ_JE>?</ZZ_JE>\n" +
                "               <LABST>?</LABST>\n" +
                "               <INSMK_C>?</INSMK_C>\n" +
                "               <BLYY1>?</BLYY1>\n" +
                "               <BLYY2>?</BLYY2>\n" +
                "               <XCHPF_RA>?</XCHPF_RA>\n" +
                "               <XCHPF_RC>?</XCHPF_RC>\n" +
                "               <SERNP>?</SERNP>\n" +
                "               <BUTTON_SER>?</BUTTON_SER>\n" +
                "               <ROWCOLOR>?</ROWCOLOR>\n" +
                "               <PLNBEZ_C>?</PLNBEZ_C>\n" +
                "               <LTXA1>?</LTXA1>\n" +
                "               <JYBAR>?</JYBAR>\n" +
                "            </item>\n" +
                "         </IT_DETAIL>\n" +
                "         <IV_BUDAT>?</IV_BUDAT>\n" +
                "      </urn:ZFLOWFU0001>\n" +
                "   </soap:Body>\n" +
                "</soap:Envelope>";
        return strSoapParameter;
    }

}
