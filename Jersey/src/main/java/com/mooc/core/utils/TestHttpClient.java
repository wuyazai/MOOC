package com.mooc.core.utils;

import com.alibaba.fastjson.JSONObject;

/**
 * author: liKun
 * createDate: 2021/3/27 20:39
 * @author likun
 */
public class TestHttpClient {

    public static void main(String[] args) {
        String url = "https://139.9.75.159:8000/sap/bc/srt/rfc/sap/zws_zflowfu0001/300/zws_zflowfu0001/zws_zflowfu0001";
        String returnData = HttpClient.doPostSoap(url, WebServiceXmlToJson.getParameter(), "");
        JSONObject jsonObject = null;
        try {
            jsonObject = WebServiceXmlToJson.xml2Json(returnData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(jsonObject);
    }
}

