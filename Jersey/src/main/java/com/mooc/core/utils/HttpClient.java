package com.mooc.core.utils;

import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * @author likun
 */
public class HttpClient {

    private final static String CONTENT_TYPE_TEXT_JSON = "text/json";

    public static String doPostSoap(String url, String soap, String soapAction) {
        // 响应的字符串
        String retStr = "";

        // HttpClient
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        CloseableHttpClient closeableHttpClient = httpClientBuilder.build();

        HttpPost httpPost = new HttpPost(url);

        try {
            // 构建请求头
            httpPost.setHeader("Authorization", "Basic " + Base64.getUrlEncoder().encodeToString(("username" + ":" + "password").getBytes()));
            httpPost.setHeader("Content-Type", "application/soap+xml;charset=UTF-8;");
            // 构建请求正文
            StringEntity data = new StringEntity(soap, StandardCharsets.UTF_8);
            httpPost.setEntity(data);
            // 执行
            CloseableHttpResponse response = closeableHttpClient.execute(httpPost);
            HttpEntity httpEntity = response.getEntity();
            if (httpEntity != null) {
                // 打印响应内容
                retStr = EntityUtils.toString(httpEntity, "UTF-8");
                System.err.println("response:" + retStr);
            }
            // 释放资源
            closeableHttpClient.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retStr;
    }
}
